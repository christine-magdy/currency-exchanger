import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { ApiResponse } from '../../base/api-response';
import { CurrencyService } from '../../services/currency.service';

@Component({
  selector: 'app-convertor',
  templateUrl: './convertor.component.html',
  styleUrls: ['./convertor.component.scss'],
})
export class ConvertorComponent implements OnInit {
  /** Input values */
  @Input() fromCurrency: any;
  @Input() fromKey: any;
  @Input() toKey: any;
  @Input() toCurrency: any;
  @Input() amount: any;
  @Input() convertResult: any;
  @Input() currencyFullName: any;
  @Input() disabledFromCurrency: boolean = false;
  @Input() fromCurrencySelected: any;
  @Input() toCurrencySelected: any;
  /** Output values to send it to History Component */
  @Output() fromCurrencyV: EventEmitter<number> = new EventEmitter();
  @Output() toCurrencyV: EventEmitter<number> = new EventEmitter();
  @Output() amountV: EventEmitter<number> = new EventEmitter();
  @Output() convertResultV: EventEmitter<number> = new EventEmitter();
  @Output() fromKeyV: EventEmitter<any> = new EventEmitter();
  @Output() toKeyV: EventEmitter<any> = new EventEmitter();
  @Output() fromCurrencySelectedV: EventEmitter<any> = new EventEmitter();
  @Output() toCurrencySelectedV: EventEmitter<any> = new EventEmitter();
  /** Array of Currencies */
  currencies: any[] = [];

  constructor(
    private currencyService: CurrencyService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    /** Call This function on the initializing the Component */
    this.getAllCurrencies();
  }

  /** Send From, To, Amount and result Values to History Component */
  sendValues() {
    this.fromCurrencyV.emit(this.fromCurrencySelected);
    this.toCurrencyV.emit(this.toCurrencySelected);
    // this.toCurrencySelectedV.emit(this.toCurrencySelected)
    this.amountV.emit(this.amount);
    this.convertResultV.emit(this.convertResult);
    this.fromKeyV.emit(this.fromKey);
    this.toKeyV.emit(this.toKey);
  }
  /** Get All Currencies from Latest API */
  getAllCurrencies() {
    this.currencyService
      .getAllCurrencies(environment.access_key)
      .subscribe((res: ApiResponse) => {
        if (!res || !res.rates) {
          this.toastr.error('Failed to get currency rates!');
          return;
        } else {
          const currencies: any[] = [];
          Object.keys(res.rates).forEach((currencyKey) => {
            const currencyValue = res.rates[currencyKey];
            currencies.push({
              key: currencyKey,
              value: currencyValue,
            });
          });
          this.currencies = currencies;
          this.fromCurrencySelected = this.currencies.find(
            (key) => key.key == this.fromKey
          );
          this.toCurrencySelected = this.currencies.find(
            (key) => key.key == this.toKey
          );
        }
      });
  }
  /** Swipe between 2 Selection (From and To) */
  swipeBetweenFromToValue(from: any, to: any) {
    this.fromCurrencySelected = to;
    this.toCurrencySelected = from;
    this.convertResult = '';
  }

  /** Convert The currencies using Conver API */
  convert(toCurrencySelected: any) {
    if (!this.amount) {
      this.toastr.error('Please add Amount First!');
    } else {
      this.toCurrencyV.emit(toCurrencySelected?.key);
      this.convertResult = this.toCurrencySelected.value * this.amount;
    }
  }

  goToDetails() {
    this.router.navigate([
      '/currency-details/currency-details',
      this.fromCurrencySelected?.key,
      this.fromCurrencySelected?.key,
      this.toCurrencySelected?.key,
      this.amount,
      this.convertResult,
    ]);
  }

  onchageToValue(value: any) {
    this.toCurrencySelected = value;
    this.toCurrencySelectedV.emit(this.toCurrencySelected?.key);
  }
}
