export interface ApiResponse {
    success?: boolean;
    timestamp?: number;
    base?: string;
    date?: Date;
    rates?: any;
  }
