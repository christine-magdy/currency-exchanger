import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ConvertorComponent } from './components/convertor/convertor.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';





@NgModule({
  declarations: [ConvertorComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
  ],
  exports: [ConvertorComponent ],
  providers: [DatePipe],
  entryComponents: []
})
export class SharedModule { }
