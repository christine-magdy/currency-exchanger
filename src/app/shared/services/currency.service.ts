import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseService } from '../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class CurrencyService extends BaseService<any>{
    env: any;
    constructor(injector: Injector) {
        super(injector);
        this.env = environment;
    }
    /** Get All currencies */
    getAllCurrencies(access_key: string) {
        return this.getAll(`latest?access_key=${access_key}`);
    }
    /** Convert Curreny using API */
    convertCurrency(access_key?: string, from?: string, to?: string, amount?: number){
        return this.getAll(`convert?access_key${access_key}&from=${from}&to=${to}&amount=${amount}`)
    }
    /** Get History for Specific Currency */
    getHistoryForCurrency(date: any, access_key?: string, symbols?: any){
        return this.getAll(`${date}?access_key=${access_key}&symbols=${symbols}`)
    }
    /** Get top 9 using This API with static Symbols from Network about Top Currencies */
    getTopNineCurrencies(access_key?: string, symbol1?: any, symbol2?: any, symbol3?: any, symbol4?: any, symbol5?: any, symbol6?: any, symbol7?: any, symbol8?: any, symbol9?: any) {
        return this.getAll(`latest?access_key=${access_key}&symbols=${symbol1},${symbol2},${symbol3},${symbol4},${symbol5},${symbol6},${symbol7},${symbol8},${symbol9}`);
    }
}
