import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiResponse } from 'src/app/shared/base/api-response';
import { CurrencyService } from 'src/app/shared/services/currency.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-top-header',
  templateUrl: './top-header.component.html',
  styleUrls: ['./top-header.component.scss']
})
export class TopHeaderComponent implements OnInit {
  today = new Date();
  fromCurrencyValue: any;
  USDcurrencyResult: any;
  GBPcurrencyResult: any;
  constructor(private currencyService: CurrencyService, private datePipe: DatePipe,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    // if (this.USDcurrencyResult || this.GBPcurrencyResult) {
      this.getCurrentValueForCurrency();
    // }
  }

  getCurrentValueForCurrency()  {
    let today = this.datePipe.transform(this.today, 'yyyy-MM-dd');
    this.currencyService.getHistoryForCurrency(today, environment.access_key, 'USD').subscribe((res: ApiResponse) => {
      if (!res || !res.rates) {
        this.toastr.error(
          `Failed to get currency rates!`
        );
        return;
      } else {
        const obj = res.rates;
        let value = Object.values(obj);
        this.fromCurrencyValue = value;
        this.USDcurrencyResult = 1 * this.fromCurrencyValue[0];
      }
    })
    
    this.currencyService.getHistoryForCurrency(today, environment.access_key, 'GBP').subscribe((res: ApiResponse) => {
      if (!res || !res.rates) {
        this.toastr.error(
          `Failed to get currency rates!`
        );
        return;
      } else {
        const obj = res.rates;
        let value = Object.values(obj);
        this.fromCurrencyValue = value;
        this.GBPcurrencyResult = 1 * this.fromCurrencyValue[0];
      }
    })
  }
}
