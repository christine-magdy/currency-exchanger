import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { ContentComponent } from './content/content.component';
import { SharedModule } from '../shared/shared.module';
import { HistoryComponent } from './content/history/history.component';
import { TopHeaderComponent } from './layout/top-header/top-header.component';



@NgModule({
  declarations: [LayoutComponent, ContentComponent, HistoryComponent, TopHeaderComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ],
})
export class HomeModule { }
