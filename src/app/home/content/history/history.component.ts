import { DatePipe, SlicePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiResponse } from 'src/app/shared/base/api-response';
import { CurrencyService } from 'src/app/shared/services/currency.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnInit {
  fromCurrency: any;
  toCurrency: any;
  amount: any;
  fromCurrencySelected: any;
  toCurrencySelected: any;

  today = new Date();
  yesterday = new Date(this.today);
  lastWeek = new Date();

  todayCurrency: any[] = [];
  yesterdayCurrency: any[] = [];
  lastWeekCurrency: any[] = [];
  lastYearCurrency: any[] = [];

  /** Top 9 Currencies: USD, EUR, JPY, GBP, CHF, CAD, AUD, NZD, ZAR */
  curriences: any[] = [];
  constructor(
    private currencyService: CurrencyService,
    private datePipe: DatePipe, private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    if(this.toCurrencySelected){
      this.getHistory();

      this.getTopNineCurrencies();
    }
   
  }

  /** Get From Currency from Convertor Component */
  getFromCurrency(fromCurrency: any) {
    this.fromCurrencySelected = fromCurrency;
    this.getTopNineCurrencies();
  }

  /** Get To Currency from Convertor Component */
  getToCurrency(toCurrency: any) {
    this.toCurrencySelected = toCurrency;
    this.getHistory();
    this.getTopNineCurrencies();
  }

  /** Get Amount Value from Convertor Component */
  getAmount(amount: any) {
    this.amount = amount;
  }

  /** Get History about TO Curreny */
  getHistory() {
    let today = this.datePipe.transform(this.today, 'yyyy-MM-dd');
    this.currencyService
      .getHistoryForCurrency(
        today,
        environment.access_key,
        this.toCurrencySelected?.key
      )
      .subscribe((res: ApiResponse) => {
        if (res.rates) {
          const currencies: any[] = [];
          Object.keys(res.rates).forEach((currencyKey) => {
            const currencyValue = res.rates[currencyKey];
            currencies.push({
              key: currencyKey,
              value: currencyValue,
            });
          });
          this.todayCurrency = currencies;
        }
      });

    let yesterday = this.yesterday.setDate(this.yesterday.getDate() - 1);
    let yesterDayFormat = this.datePipe.transform(yesterday, 'yyyy-MM-dd');

    this.currencyService
      .getHistoryForCurrency(
        yesterDayFormat,
        environment.access_key,
        this.toCurrencySelected?.key
      )
      .subscribe((res: ApiResponse) => {
        if (res.rates) {
          const currencies: any[] = [];
          Object.keys(res.rates).forEach((currencyKey) => {
            const currencyValue = res.rates[currencyKey];
            currencies.push({
              key: currencyKey,
              value: currencyValue,
            });
          });
          this.yesterdayCurrency = currencies;
        }
      });

    let lastWeek = this.lastWeek.setDate(this.lastWeek.getDate() - 7);
    let lastWeekFormat = this.datePipe.transform(lastWeek, 'yyyy-MM-dd');

    this.currencyService
      .getHistoryForCurrency(
        lastWeekFormat,
        environment.access_key,
        this.toCurrencySelected?.key
      )
      .subscribe((res: ApiResponse) => {
        if (res.rates) {
          const currencies: any[] = [];
          Object.keys(res.rates).forEach((currencyKey) => {
            const currencyValue = res.rates[currencyKey];
            currencies.push({
              key: currencyKey,
              value: currencyValue,
            });
          });
          this.lastWeekCurrency = currencies;
        }
      });

    const currentYear = new Date().getFullYear();
    const previousYear = currentYear - 1;
    let lastYear = this.datePipe.transform(this.today, `${previousYear}-MM-dd`);

    this.currencyService
      .getHistoryForCurrency(
        lastYear,
        environment.access_key,
        this.toCurrencySelected?.key
      )
      .subscribe((res: ApiResponse) => {
        if (res.rates) {
          const currencies: any[] = [];
          Object.keys(res.rates).forEach((currencyKey) => {
            const currencyValue = res.rates[currencyKey];
            currencies.push({
              key: currencyKey,
              value: currencyValue,
            });
          });
          this.lastYearCurrency = currencies;
        }
      });
  }

  /** Get top 9 Currencies */
  getTopNineCurrencies() {
    this.currencyService
      .getTopNineCurrencies(
        environment.access_key,
        'USD',
        'EUR',
        'JPY',
        'GBP',
        'CHF',
        'CAD',
        'AUD',
        'NZD',
        'ZAR'
      )
      .subscribe((res: ApiResponse) => {
        if (!res || !res.rates) {
          this.toastr.error('Failed to get currency rates.')
          return;
        } else {
          const currencies: any[] = [];
          Object.keys(res.rates).forEach((currencyKey) => {
            const currencyValue = res.rates[currencyKey];
            currencies.push({
              key: currencyKey,
              value: currencyValue,
            });
          });
          this.curriences = currencies;
        }
      });
  }
}
