import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from '../home/layout/layout.component';
import { CurrencyDetailsComponent } from './currency-details/currency-details.component';

const routes: Routes = [
  {
    path: '' ,
    component: LayoutComponent,
    children: [
      {
        path: 'currency-details/:currency/:fromKey/:toKey/:amount/:convertResult',
        component: CurrencyDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencyDetailsRoutingModule { }
