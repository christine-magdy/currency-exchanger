import { DatePipe } from '@angular/common';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { ApiResponse } from 'src/app/shared/base/api-response';
import { CurrencyService } from 'src/app/shared/services/currency.service';
import { environment } from 'src/environments/environment';
import CurrencyList from 'currency-list';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-currency-details',
  templateUrl: './currency-details.component.html',
  styleUrls: ['./currency-details.component.scss'],
})
export class CurrencyDetailsComponent implements OnInit, OnChanges {
  currencies: any[] = [];
  date = new Date();
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  /** Contains Every Last Day of each Month */
  Jan: any;
  Feb: any;
  Mar: any;
  Apr: any;
  May: any;
  Jun: any;
  Jul: any;
  Aug: any;
  Sep: any;
  Oct: any;
  Nov: any;
  Dec: any;

  /** Value of Currency for each Month */
  JanValue: any;
  FebValue: any;
  MarValue: any;
  AprValue: any;
  MayValue: any;
  JunValue: any;
  JulValue: any;
  AugValue: any;
  SepValue: any;
  OctValue: any;
  NovValue: any;
  DecValue: any;

  /** Array to push values on it */
  public monthsRatesArrayFromCurrency: any[] = [];
  public monthsRatesArrayToCurrency: any[] = [];

  /** Array of Dates */
  public dateArray: any[] = [];

  currency: any;
  currencyFullName: any;
  public fromCurrency: any;
  fromKey: any;
  fromValue: any;
  public toKey: any;
  toValue: any;
  toCurrency: any;
  amount: any;
  convertResult: any;
  fromCurrencyChartDataReady: boolean | undefined;
  toCurrencyChartDataReady: boolean | undefined;

  fromCurrencySelected: any;
  toCurrencySelected: any;

  /** Chart Setup */
  public lineChartDataFromCurrency: ChartDataSets[] | undefined;
  public lineChartDataToCurrency: ChartDataSets[] | undefined;

  lineChartLabels: Label[] = this.months;
  lineChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
  };
  lineChartColors: Color[] = [
    {
      borderColor: '#d0d1e6',
    },
  ];
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(
    private router: ActivatedRoute,
    private route: Router,
    private currencyService: CurrencyService,
    private datePipe: DatePipe,
    private location: Location,
    private toastr: ToastrService
  ) {
    this.router.params.subscribe((res) => {
      this.currency = res['currency'];
      this.toKey = res['toKey'];
      this.fromKey = res['fromKey'];
      this.amount = res['amount'];
      this.convertResult = res['convertResult'];
    });
    this.route.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    this.getHistoryOfSelectedCurrency();

    /** Get Full Name using currency-list That i installed it from npm i currency-list */
    if (!CurrencyList.get(this.fromKey)?.name) {
      console.error(
        'Failed to get currency full name, Full name not found here.'
      );
      return;
    } else {
      this.currencyFullName = CurrencyList.get(this.fromKey)?.name;
    }
  }

  /** Get From Currency from Convertor Component */
  getFromCurrency(fromCurrency: any) {
    this.fromCurrencySelected = fromCurrency;
  }
  /** Get Key for FROM Currency from Convertor Component */
  getFromKey(fromKey: any) {
    this.fromKey = fromKey;
  }
  /** Get From Value */
  getFromValue(fromValue: any) {
    this.fromValue = fromValue;
  }
  /** Get To Currency from Convertor Component */
  getToCurrency(toCurrency: any) {
    this.toCurrencySelected = toCurrency;
    if(this.toCurrencySelected?.key != this.toKey){
      this.toKey = toCurrency;

      this.location.replaceState(`/currency-details/currency-details/${this.fromKey}/${this.fromKey}/${this.toKey}/${this.amount}/${this.convertResult}`)
      location.reload()
    }
    else {
      this.location.replaceState(`/currency-details/currency-details/${this.fromKey}/${this.fromKey}/${this.toKey}/${this.amount}/${this.convertResult}`)
      location.reload()
    }
    
  }
  /** Get Amount Value from Convertor Component */
  getAmount(amount: any) {
    this.amount = amount;
  }
  /** Get Result Value from Convertor Component */
  getResult(convertResult: any) {
    this.convertResult = convertResult;
  }
  getSelectedFrom(from?: any) {
    this.fromCurrency = from;
  }

  /** Get History for Specific Currency
   * Monthly rate is calculated based on rate in last day of that month */
  getHistoryOfSelectedCurrency() {
    const currentYear = new Date().getFullYear();
    const previousYear = currentYear - 1;

    let JanLast = new Date(previousYear, 1 - 1 + 1, 0);
    this.Jan = this.datePipe.transform(JanLast, `${previousYear}-MM-dd`);

    let FebLast = new Date(previousYear, 2 - 1 + 1, 0);
    this.Feb = this.datePipe.transform(FebLast, `${previousYear}-MM-dd`);

    let MarLast = new Date(previousYear, 3 - 1 + 1, 0);
    this.Mar = this.datePipe.transform(MarLast, `${previousYear}-MM-dd`);

    let AprLast = new Date(previousYear, 4 - 1 + 1, 0);
    this.Apr = this.datePipe.transform(AprLast, `${previousYear}-MM-dd`);

    let MayLast = new Date(previousYear, 5 - 1 + 1, 0);
    this.May = this.datePipe.transform(MayLast, `${previousYear}-MM-dd`);

    let JunLast = new Date(previousYear, 6 - 1 + 1, 0);
    this.Jun = this.datePipe.transform(JunLast, `${previousYear}-MM-dd`);

    let JulLast = new Date(previousYear, 7 - 1 + 1, 0);
    this.Jul = this.datePipe.transform(JulLast, `${previousYear}-MM-dd`);

    let AugLast = new Date(previousYear, 8 - 1 + 1, 0);
    this.Aug = this.datePipe.transform(AugLast, `${previousYear}-MM-dd`);

    let SepLast = new Date(previousYear, 9 - 1 + 1, 0);
    this.Sep = this.datePipe.transform(SepLast, `${previousYear}-MM-dd`);

    let OctLast = new Date(previousYear, 10 - 1 + 1, 0);
    this.Oct = this.datePipe.transform(OctLast, `${previousYear}-MM-dd`);

    let NovLast = new Date(previousYear, 11 - 1 + 1, 0);
    this.Nov = this.datePipe.transform(NovLast, `${previousYear}-MM-dd`);

    let DecLast = new Date(previousYear, 12 - 1 + 1, 0);
    this.Dec = this.datePipe.transform(DecLast, `${previousYear}-MM-dd`);
    /** Push all Dates to Date Array */
    this.dateArray.push(
      this.Jan,
      this.Feb,
      this.Mar,
      this.Apr,
      this.May,
      this.Jun,
      this.Jul,
      this.Aug,
      this.Sep,
      this.Oct,
      this.Nov,
      this.Dec
    );

    this.currencyService
    .getHistoryForCurrency(this.dateArray[0], environment.access_key, this.fromKey).subscribe((result: ApiResponse) => {
      if (!result || !result.rates) {
        this.toastr.error(
          `Failed to get History of ${this.fromKey} Currency!`
        );
        return;
      } else {
         /** Iterate to Date Array */
    for (let date of this.dateArray) {
      /** Calling History API with date of each month and FROM Currency */
      //  date: any, access_key?: string, symbols?: any
      this.currencyService
        .getHistoryForCurrency(date, environment.access_key, this.fromKey)
        .subscribe((res: ApiResponse) => {
          if (!res || !res.rates) {
            this.toastr.error(
              `Failed to get History of ${this.fromKey} Currency! for this Date ${date}`
            );
            return;
          } else {
            const obj = res.rates;
            let value = Object.values(obj);
            this.monthsRatesArrayFromCurrency.push(Math.round(value as any));
            this.lineChartDataFromCurrency = [
              {
                data: this.monthsRatesArrayFromCurrency,
                label: `History of ${this.fromKey} for Last Year`,
              },
            ];
            this.fromCurrencyChartDataReady = true;
          }
        });

      /** Calling History API with date of each month and TO Currency */
      //  date: any, access_key?: string, symbols?: any
      this.currencyService
        .getHistoryForCurrency(date, environment.access_key, this.toKey)
        .subscribe((res: ApiResponse) => {
          if (!res || !res.rates) {
            this.toastr.error(
              `Failed to get History of ${this.toKey} Currency! for this Date ${date}`
            );
            return;
          } else {
            const obj = res.rates;
            let value = Object.values(obj);
            this.monthsRatesArrayToCurrency.push(Math.round(value as any));
            this.lineChartDataToCurrency = [
              {
                data: this.monthsRatesArrayToCurrency,
                label: `History of ${this.toKey} for Last Year`,
              },
            ];
            this.toCurrencyChartDataReady = true;
          }
        });
    }
      }
    })

   
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getHistoryOfSelectedCurrency();
    // this.toKey;
    // this.getToCurrency(this.toKey);
  }
}
