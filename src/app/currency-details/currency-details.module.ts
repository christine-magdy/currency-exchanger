import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CurrencyDetailsRoutingModule } from './currency-details-routing.module';
import { CurrencyDetailsComponent } from './currency-details/currency-details.component';
import { SharedModule } from '../shared/shared.module';
import { ChartsModule } from 'ng2-charts/ng2-charts';


@NgModule({
  declarations: [
    CurrencyDetailsComponent
  ],
  imports: [
    CommonModule,
    CurrencyDetailsRoutingModule,
    SharedModule,
    ChartsModule
  ]
})
export class CurrencyDetailsModule { }
